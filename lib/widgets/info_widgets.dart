import 'package:flutter/material.dart';
import 'package:temperature_monitoring/widgets/color_icons.dart';

class InfoWidget extends StatelessWidget {
  var iconName;
  var value;
  var color;
  var spaceBetween;
  var boxSize;
  var borderRadius;
  var textColor;
  var unitText;
  InfoWidget(this.iconName, this.value, this.color,
      {this.spaceBetween = 10.0,
      this.boxSize = 60.0,
      this.borderRadius = 20.0,
      this.textColor = Colors.black,
      this.unitText = ''});
  @override
  Widget build(BuildContext context) {
    //final size=MediaQuery.of(context).size;
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        Container(
          height: boxSize,
          width: boxSize,
          decoration: BoxDecoration(
              color: color[1],
              borderRadius: BorderRadius.circular(borderRadius)),
          child: Center(
              child: Container(
                  child: ColorIcons(
                      iconName,
                      boxSize -
                          18)) //Icon(icon,size:boxSize-22,color: color[0],)),
              ),
        ),
        SizedBox(
          height: spaceBetween,
        ),
        Container(
            height: 40,
            child: Text(
              value != null ? "$value" + unitText : "NA",
              style: TextStyle(color: textColor),
            ))
      ],
    );
  }
}
