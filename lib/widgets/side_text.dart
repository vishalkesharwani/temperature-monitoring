import 'package:flutter/material.dart';

import 'package:temperature_monitoring/utils/utils.dart' as utils;

class SideText extends StatelessWidget {
  var text;
  var fontSize;
  var color;
  var gradient;
  SideText({
    this.text,
    this.fontSize,
    this.gradient,
    this.color,
  });
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.centerLeft,
      child: Text(
        text,
        style: TextStyle(
            foreground: gradient
                ? (Paint()..shader = utils.linearGradient)
                : (Paint()..color = color),
            fontSize: fontSize),
      ),
    );
  }
}
