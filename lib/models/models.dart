class City {
  var id;
  var name;
  var adminDistrict;
  var country;
  var coordinates;
  var temporary;
  var timeZoneOffset;
  City(
      {this.name,
      this.adminDistrict,
      this.country,
      this.coordinates,
      this.id,
      this.temporary,
      this.timeZoneOffset});
  Map<String, dynamic> toMap() {
    return {
      "id": id,
      'name': name,
      'adminDistrict': adminDistrict,
      'country': country,
      'coordinates': coordinates.toString(),
      'timeZoneOffset': timeZoneOffset
    };
  }
}

class CurrentWeather {
  var id;
  var timestamp;
  var sunrise;
  var sunset;
  var temp;
  var feelsLike;
  var pressure;
  var humidity;
  var dewPoint;
  var uvi;
  var clouds;
  var visibility;
  var windSpeed;
  var windDirection;
  var rain;
  var description;
  CurrentWeather(
      {this.id,
      this.rain,
      this.timestamp,
      this.sunrise,
      this.sunset,
      this.temp,
      this.feelsLike,
      this.pressure,
      this.humidity,
      this.uvi,
      this.clouds,
      this.description,
      this.dewPoint,
      this.visibility,
      this.windDirection,
      this.windSpeed});
  Map<String, dynamic> toMap() {
    return {
      "id": id,
      "timestamp": timestamp,
      'sunrise': sunrise,
      'sunset': sunset,
      'temp': temp,
      'feelsLike': feelsLike,
      'pressure': pressure,
      'humidity': humidity,
      'dewPoint': dewPoint,
      'uvi': uvi,
      'clouds': clouds,
      'visibility': visibility,
      'windSpeed': windSpeed,
      'windDirection': windDirection,
      'rain': rain,
      'description': description,
    };
  }
}

//CREATE TABLE ${id}_daily( minTemp REAL,nightFeelsLike REAL,nightTemp REAL,rain REAL,clouds REAL,dayFeelsLike REAL,dayTemp REAL,description TEXT,dewPoint REAL,humidity REAL,maxTemp REAL,timestamp REAL,sunrise REAL,sunset REAL,temp REAL,pressure REAL,uvi REAL,visibility REAL,windDirection REAL,windSpeed REAL)
class DailyWeather {
  var timestamp;
  var sunrise;
  var sunset;
  var dayTemp;
  var nightTemp;
  var minTemp;
  var maxTemp;
  var dayFeelsLike;
  var nightFeelsLike;
  var pressure;
  var humidity;
  var dewPoint;
  var uvi;
  var clouds;
  var visibility;
  var windSpeed;
  var rain;
  var windDirection;
  var description;
  DailyWeather(
      {this.minTemp,
      this.nightFeelsLike,
      this.nightTemp,
      this.rain,
      this.clouds,
      this.dayFeelsLike,
      this.dayTemp,
      this.description,
      this.dewPoint,
      this.humidity,
      this.maxTemp,
      this.timestamp,
      this.sunrise,
      this.sunset,
      this.pressure,
      this.uvi,
      this.visibility,
      this.windDirection,
      this.windSpeed});
  Map<String, dynamic> toMap() {
    return {
      "timestamp": timestamp,
      'sunrise': sunrise,
      'sunset': sunset,
      'dayTemp': dayTemp,
      'nightTemp': nightTemp,
      'minTemp': minTemp,
      'maxTemp': maxTemp,
      'dayFeelsLike': dayFeelsLike,
      'nightFeelsLike': nightFeelsLike,
      'pressure': pressure,
      'humidity': humidity,
      'dewPoint': dewPoint,
      'uvi': uvi,
      'clouds': clouds,
      'visibility': visibility,
      'windSpeed': windSpeed,
      'rain': rain,
      'windDirection': windDirection,
      'description': description,
    };
  }
}

//CREATE TABLE ${id}_hourly(timestamp REAL,temp REAL,feelsLike REAL,pressure REAL,humidity REAL,clouds REAL,description TEXT,dewPoint REAL,visibility REAL,windDirection REAL,windSpeed REAL)
class HourlyWeather {
  var timestamp;
  var temp;
  var feelsLike;
  var pressure;
  var humidity;
  var dewPoint;
  var clouds;
  var visibility;
  var windSpeed;
  var windDirection;
  var description;
  HourlyWeather(
      {this.timestamp,
      this.temp,
      this.feelsLike,
      this.pressure,
      this.humidity,
      this.clouds,
      this.description,
      this.dewPoint,
      this.visibility,
      this.windDirection,
      this.windSpeed});
  Map<String, dynamic> toMap() {
    return {
      "timestamp": timestamp,
      'temp': temp,
      'feelsLike': feelsLike,
      'pressure': pressure,
      'humidity': humidity,
      'dewPoint': dewPoint,
      'clouds': clouds,
      'visibility': visibility,
      'windSpeed': windSpeed,
      'windDirection': windDirection,
      'description': description,
    };
  }
}
